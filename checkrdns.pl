#!/usr/bin/perl

use strict;
use  Net::DNS;

use Env qw($RES_NAMESERVERS);
use Getopt::Std;
use Socket;

#######################################################################
# params
my %opts =(
);

#######################################################################
# constants
$\ = "\n";
$| = 1;

my $res;
my $dnssrv;
my @ipranges;
my $verbose;
my @workranges;
my $r;

#######################################################################
local $SIG{TERM} = sub { atexit("SIGTERM");};
local $SIG{INT}  = sub { atexit("SIGINT"); };

sub atexit {
  printf "\033[m";
  print "\n\033[1;31m*** Interrupted by $_[0] ***\033[m\n";
  exit;
}

#sub iplist2range {
#  my @ipl = @_;
#  my ($ip, $f, $cur_net, $pip, @r);
#  for $ip (sort { inet_aton($a) cmp inet_aton($b) } @ipl) {
#    if ($ip =~ /^(\d{1,3}\.\d{1,3}\.\d{1,3})\.(\d{1,3})$/) {
#      $cur_net = $1;
#      $f = $2 unless $f;
#      $pip = ($2-1) unless $pip;
#      if ($pip != ($2-1)) {
#         push(@r, "$cur_net.$f-$pip");
#        $f = $2;
#      }
#      $pip = $2;
#    } else {
#       die("Error: $ip is not IP address");
#    }
#  }
# push(@r, "$cur_net.$f-$pip");
# return @r;
#}

sub iplist2range {
  my @ipl = @_;
  my ($ip, $f, $cur_net, $pip, @r);
  for $ip (sort { inet_aton($a) cmp inet_aton($b) } @ipl) {
    if ($ip =~ /^(\d{1,3}\.\d{1,3}\.\d{1,3})\.(\d{1,3})$/) {
#      print "$ip pip=$pip f=$f";
      $f = $2 unless $f;
      if ($cur_net && ($cur_net ne $1)) {
         push(@r, "$cur_net.$f-$pip");
#         print "$cur_net.$f-$pip";
           undef($pip);
	   $cur_net = $1;
	   $f = $2;
	   next;
      }
      $cur_net = $1;
#      print "$ip pip=$pip f=$f ==$2" if ((defined $pip) & ($pip != ($2-1)));
      if (defined $pip & ($pip != ($2-1))) {
         push(@r, "$cur_net.$f-$pip");
#	 print "$cur_net.$f-$pip";
         undef($pip);
        $f = $2;
      }
      $pip = $2 unless $pip;
      $pip = $2;
    } else {
       die("Error: $ip is not IP address");
    }
  }
 push(@r, "$cur_net.$f-$pip");
#         print "$cur_net.$f-$pip";
 return @r;
}


sub cidr2range {
  my $cidr = shift(@_);
  my ($s, $e, $i, $r);
  if ($cidr =~ /^(\d{1,3}\.\d{1,3})\.(\d{1,3})\.(\d{1,3})\/(\d{1,2})$/) {
    if ($4 > 24) {
     $s = inet_ntoa(inet_aton("$1.$2.$3") & pack("N",(0xFFFFFFFF << (32-$4))));
     $e = $3 | ~ (0xFFFFFFFF << (32-$4));
     return "$s-$e";
    } else {
      for ($i = 0; $i < (1<<(24-$4)); $i++) {
	  $r = $r . sprintf "%s.%s.1-254 ",$1,$2+$i;
#	  printf "%s.%s.1-254\n ",$1,$2+$i;
      }
    }
  }
  return $r;
}

sub range2iplist {
  my @rl =  @_;
  my (@ipl,$n);
  my ($i,$j, $ip, $f, $l, $r, $k);
  for $r (@rl) {
    next ($r) if ($r =~ /^(\d{1,3}\.\d{1,3}\.\d{1,3})\.(\d{1,3})$/);
    foreach $i (split(' ',$r)) {
     $i = cidr2range($i) if ($i =~ /^(\d{1,3}\.\d{1,3}\.\d{1,3})\.(\d{1,3})\/(\d{1,2})$/);

     foreach $k (split(' ',$i)) {
       if ($k =~ /^(\d{1,3}\.\d{1,3}\.\d{1,3})\.(\d{1,3})\-(\d{1,3})$/) {
         ($n, $f, $l) = ($1, $2, $3);
         for($j = $f; $j <= $l; $j++) {
           push(@ipl,("$n.$j"));
         }
       }
     }


    }
  }
  return @ipl;
}

sub myresolve {
  my $res = shift(@_);
  my $i = shift(@_);
  my %r;
  my $packet = $res->query($i);
  if ($packet) {
     foreach my $rr ($packet->answer) {
        next unless (($rr->type eq "A") || ($rr->type eq "PTR"));
        $r{$rr->rdatastr}=1;
#	print $rr->rdatastr;
#	$rr[0]->rdatastr
      }
   } else {
       return () if ($res->errorstring eq "NXDOMAIN");
       print "query failed: ", $res->errorstring;
       return ();
   }
   return %r;
}

sub usage {
  print "Usage: checkrdns.pl [-v] [-d <dns server>] <iprange>";
  exit;
}

sub checkrange {
  my $r = shift(@_);
  my ($ip, %addr, %ptr, $domain, @wr);
  my @ipl = range2iplist($r);

  die "Incorrect iprange" unless (@ipl);

  print "\033[1mChecking RDNS of range $r\033[m";
  for $ip (@ipl) {
    %ptr = myresolve($res,$ip);
    if (scalar keys %ptr >1)   {print "\033[1;33mWarning\033[m: $ip resolved to many PTR - " . join(', ',keys %ptr); next; }
    if (scalar keys %ptr == 0) {print "\033[1;33mWarning\033[m: $ip has no rdns"; next; }
    $domain = (keys %ptr)[0];
    %addr = myresolve($res, $domain);
    if (not exists $addr{$ip}) {
       printf ("\033[31;1mError    IP %-18s: -> %-30s -> %s\033[m\n", $ip, $domain, join(', ',sort keys %addr));
#       print "Error: $ip -> $domain -> " . join(', ',sort keys %addr);
       next;
    }
    printf ("Checking IP %-18s: -> %-30s -> %s: OK\n", $ip, $domain, join(', ',sort keys %addr)) if ($verbose);
    push(@wr, $ip);
  }
#  print iplist2range(@wr);
  return join(';',iplist2range(@wr));

}
#######################################################################
# MAIN

usage() if (($ARGV[0] =~ /^--help$/));

getopts('vd:',\%opts);
$verbose=$opts{'v'};
$dnssrv=$opts{'d'};
system('rndc flush') unless ($dnssrv);

#print(cidr2range($ARGV[0]));
#exit;

if ($ARGV[0]) {
  @ipranges = @ARGV;
} else {
  print "Please input ranges for check:\033[1;36m";
  while (<>) {
    push(@ipranges, $1) if (/^\s*(\d{1,3}\.\d{1,3}\.\d{1,3}\..*\d{1,3})\s*/);
  }
}
print "\033[m";
#$dnssrv  = $ARGV[1];

#usage() if ($ARGV[0] !~ /(\d+\.\d+\.\d+)\./);

$res = Net::DNS::Resolver->new;
$res->nameservers($dnssrv) if (defined $dnssrv);

for $r (@ipranges) {
  push (@workranges,checkrange($r));
}

print "\n\033[1mWorked ranges:\033[m";
for $r (@workranges) {
  print "\033[1;32m$r\033[m";
}
